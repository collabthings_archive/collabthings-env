psj=$(ps aux | grep java | grep -v grep)
while [ ! -z "${psj}" ]; do
	psj=$(ps aux | grep java | grep -v grep)
	echo ${psj}
	killall java
	sleep 5
done

if [ ! -d /opt/maven ]; then
	apt-get update
	apt-get -qy install joe unzip git openjdk-8-jdk nginx 

	cd /opt

	wget -nc http://archive.apache.org/dist/maven/maven-3/3.1.0/binaries/apache-maven-3.1.0-bin.tar.gz
	tar zxf apache-maven-3.1.0-bin.tar.gz
	ln -s apache-maven-3.1.0 maven
fi

#if [ -z $(which docker) ]; then
#	apt-get -qy install docker.io
#fi

export MVN_HOME=/opt/apache-maven-3.1.0
echo MAVEN_HOME $MVN_HOME

export PATH="$PATH:$MVN_HOME/bin"

if [ ! -d /etc/tomcat8 ]; then
	apt-get -qy install tomcat8
fi


if [ ! -d /etc/mongo ]; then
	apt-get -qy install mongodb
fi

sed -e "s/Xmx128m/Djava.net.preferIPv4Stack=true -Xmx1024m/g" /etc/default/tomcat8 > /tmp/tomcat8.conf
cat /tmp/tomcat8.conf > /etc/default/tomcat8

/etc/init.d/tomcat8 restart
sleep 5

mkdir /project/logs

cd /waazdoh.service
ls target/*.war
cp target/service-1.3.2-SNAPHOT.war /var/lib/tomcat8/webapps/service.war

cd /collabthings.web
ls target/*.war
cp target/web-0.0.3-SNAPHOT.war /var/lib/tomcat8/webapps/ROOT.war

while ! wget http://localhost:8080/service/admin/basicchecks -O /project/logs/checks.txt; do
	echo Waiting for port 8080
	tail /var/log/tomcat8/catalina.out; 
	sleep 5
done


while ! wget http://localhost:8080/index.html -O /project/logs/indexchecks.txt; do
	echo Waiting for index.html 8080
	tail /var/log/tomcat8/catalina.out; 
	sleep 5
done

echo Calling service

wget http://localhost:8080/service/admin/basicchecks -O /project/logs/checks.txt
cat /project/logs/checks.txt

cd

nohup java -Dservice.url=http://localhost:8080/service -cp /waazdoh.client/target/client-1.5.3-SNAPSHOT-jar-with-dependencies.jar waazdoh.client.app.AppLauncher > /project/logs/client.log 2> /project/logs/client.error.log &

netstat -na | grep tcp | grep LIST
